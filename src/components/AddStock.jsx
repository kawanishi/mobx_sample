import React from "react";
import { inject, observer } from "mobx-react";

@inject("stocks")
@observer
class AddStock extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    const { stocks } = this.props;

    const newitem = stocks.newItem;
    if (newitem && newitem.length > 0) {
      stocks.addItem(newitem);
      stocks.newItem = "";
    }
  }

  handleChange(event) {
    const { stocks } = this.props;
    const newitem = event.target && event.target.value;
    stocks.newItem = newitem.trim();
  }

  render() {
    const { stocks } = this.props;
    return (
      <form>
        <input value={stocks.newItem} onChange={this.handleChange} />
        &nbsp;
        <button onClick={this.handleClick}>登録</button>
      </form>
    );
  }
}

export default AddStock;
