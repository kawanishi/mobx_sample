import React from "react";
import { inject, observer } from "mobx-react";

@inject("stocks")
@observer

class DeleteStock extends React.Component {
  render() {
    const { stocks } = this.props;
    return (
      <span>
        <button onClick={() => stocks.removeItemById(this.props.item.id)}>削除</button>
      </span>
    );
  }
}

export default DeleteStock;
