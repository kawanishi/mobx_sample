import React from "react";
import { render } from "react-dom";
import { inject, observer } from "mobx-react";
import ChangeQuantity from "./ChangeQuantity.jsx";
import DeleteStock from "./DeleteStock.jsx";

@inject("stocks")
@observer
class StockList extends React.Component {
  render() {
    const { stocks } = this.props;
    const filteredStocks = [];
    for (let i = 0; i < stocks.items.length; i++ ) {
      if (stocks.items[i].delFlg !== true) {
        filteredStocks.push(stocks.items[i])
      }
    }
    return (
      <div>
        {filteredStocks.map(item => (
          <div>
            {item.name} <ChangeQuantity item={item}/> <DeleteStock item={item} />
          </div>
        ))}
      </div>
    );
  }
}

export default StockList;
