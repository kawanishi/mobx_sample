import React from "react";
import { render } from "react-dom";
import { Provider } from "mobx-react";
import AddStock from "./components/AddStock.jsx";
import StockList from "./components/StockList.jsx";
import StockStore from "./store/StockStore";

const stores = new StockStore();

render(
  <Provider stocks={stores}>
    <div>
      <AddStock />
      <StockList />
    </div>
  </Provider>,
  document.getElementById("root")
);
