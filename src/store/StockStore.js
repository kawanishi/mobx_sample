import { observable } from "mobx";

class StockStore {
  @observable items = [];

  @observable
  postItem = {
    id: 0,
    name: "",
    quantity: 0,
    delFlg: false
  };

  @observable newItem = "";
  @observable lastItemId = 0;

  addItem(post) {
    this.lastItemId += 1;
    this.postItem = {
      id: this.lastItemId,
      name: post,
      quantity: 0,
      delFlg: false
    };
    this.items.push(this.postItem);
  }

  increaseItemById(id) {
    this.items[id - 1].quantity += 1;
  }

  decreaseItemById(id) {
    if (this.items[id - 1].quantity > 0) {
      this.items[id - 1].quantity -= 1;
    }
  }

  removeItemById(id) {
    this.items[id - 1].delFlg = true;
  }
}

export default StockStore;
