/* eslint-disable */
import * as mobx from "mobx";
import StockStore from "../StockStore"

describe("StockStore", () => {
  let stocks;
  beforeAll(() => {
    stocks = new StockStore();
  });

  describe("items", () => {
    it("should be empty", () => {
      expect(mobx.toJS(stocks.items)).toEqual([]);
    });
  });

  describe("postItem", () => {
    it("should be initial value", () => {
      expect(stocks.postItem.id).toBe(0);
      expect(stocks.postItem.name).toBe("");
      expect(stocks.postItem.quantity).toBe(0);
      expect(stocks.postItem.delFlg).toBe(false);
    });
  });

  describe("newItem", () => {
    it("should be initial value", () => {
      expect(stocks.newItem).toBe("");
    });
  });

  describe("lastItemId", () => {
    it("should be initial value", () => {
      expect(stocks.lastItemId).toBe(0);
    });
  });

  describe("addItem()", () => {
    test("addItem", () => {
      stocks.addItem("test1");
      expect(stocks.lastItemId).toBe(1);
      expect(stocks.postItem.id).toBe(1);
      expect(stocks.postItem.name).toBe("test1");
      expect(stocks.items[0].id).toBe(1);
      expect(stocks.items[0].name).toBe("test1");
      stocks.addItem("test2");
      expect(stocks.lastItemId).toBe(2);
      expect(stocks.postItem.id).toBe(2);
      expect(stocks.postItem.name).toBe("test2");
      expect(stocks.items[1].id).toBe(2);
      expect(stocks.items[1].name).toBe("test2");
    });
  });

  describe("increaseItemById()", () => {
    test("increaseItemById", () => {
      stocks.increaseItemById(2);
      expect(stocks.items[1].quantity).toBe(1);
      expect(stocks.items[1].id).toBe(2);
      expect(stocks.items[1].name).toBe("test2");
      expect(stocks.items[1].delFlg).toBe(false);
      stocks.increaseItemById(2);
      expect(stocks.items[1].quantity).toBe(2);
    });
  });

  describe("decreaseItemById()", () => {
    test("decreaseItemById", () => {
      stocks.decreaseItemById(1);
      expect(stocks.items[0].quantity).toBe(0);
      expect(stocks.items[0].id).toBe(1);
      expect(stocks.items[0].name).toBe("test1");
      expect(stocks.items[0].delFlg).toBe(false);
      stocks.decreaseItemById(1);
      expect(stocks.items[0].quantity).toBe(0);
    });
  });

  describe("removeItemById()", () => {
    test("removeItemById", () => {
      stocks.removeItemById(1);
      expect(stocks.items[0].delFlg).toBe(true);
      stocks.removeItemById(2);
      expect(stocks.items[1].delFlg).toBe(true);
    });
  });
})
