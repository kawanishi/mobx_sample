const path = require("path");

module.exports = {
  entry: {
    index: path.join(__dirname, "src", "index.jsx")
  },
  output: {
    path: path.join(__dirname, "out"),
    filename: "[name].js"
  },
  devServer: {
    contentBase: path.join(__dirname, "out"),
    port: 3000,
    inline: true
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          plugins: ["transform-decorators-legacy", "transform-class-properties"]
        }
      }
    ]
  }
};
